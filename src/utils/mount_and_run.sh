#!/usr/bin/env bash
#
# Class: COE 1550
# Author: Neel Kowdley <nkowdley@gmail.com>
# Project 4: File Systems
# Date:04/08/2016
# File: mount_and_run.sh
# Description: A simple bash script for the CS1550 Project 4 which
# removes the old .disk, creates a new one,Then creates a screen,
# runs the disk on the screen, Then detaches the screen

echo -e "Removing Disk\n"
rm -f .disk #remove
echo -e "Creating New Disk: .disk\n"
dd bs=1K count=5K if=/dev/zero of=.disk
echo -e "\nDone creating disk"
#make a detached screen terminal and run the command, then bash
echo "Stopping screen"
killall screen
echo "Running the FS in screen"
screen -dmS project4 sh -c  './cs1550 -d testmount; exec bash'
echo "To view the FS output, use screen -r"
