/*
* Class: COE 1550
* Author: Neel Kowdley <nkowdley@gmail.com>
* Project 4: File Systems
* Date:04/08/2016
* File: cs1550.c
*/

/*
	FUSE: Filesystem in Userspace
	Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>

	This program can be distributed under the terms of the GNU GPL.
	See the file COPYING.

	gcc -Wall `pkg-config fuse --cflags --libs` cs1550.c -o cs1550
*/

#define	FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>

//size of a disk block
#define	BLOCK_SIZE 512

//we'll use 8.3 filenames
#define	MAX_FILENAME 8
#define	MAX_EXTENSION 3
/*define the BITMAP SIZE*/
#define BITMAP_SIZE BLOCK_SIZE*3
//How many files can there be in one directory?
#define	MAX_FILES_IN_DIR (BLOCK_SIZE - (MAX_FILENAME + 1) - sizeof(int)) / \
	((MAX_FILENAME + 1) + (MAX_EXTENSION + 1) + sizeof(size_t) + sizeof(long))

//How much data can one block hold?
#define	MAX_DATA_IN_BLOCK (BLOCK_SIZE - sizeof(unsigned long))


struct cs1550_directory_entry
{
	int nFiles;	//How many files are in this directory.
				//Needs to be less than MAX_FILES_IN_DIR

	struct cs1550_file_directory
	{
		char fname[MAX_FILENAME + 1];	//filename (plus space for nul)
		char fext[MAX_EXTENSION + 1];	//extension (plus space for nul)
		size_t fsize;					//file size
		long nStartBlock;				//where the first block is on disk
	} __attribute__((packed)) files[MAX_FILES_IN_DIR];	//There is an array of these

	//This is some space to get this to be exactly the size of the disk block.
	//Don't use it for anything.
	char padding[BLOCK_SIZE - MAX_FILES_IN_DIR * sizeof(struct cs1550_file_directory) - sizeof(int)];
} ;

typedef struct cs1550_directory_entry cs1550_directory_entry;

#define MAX_DIRS_IN_ROOT (BLOCK_SIZE - sizeof(int)) / ((MAX_FILENAME + 1) + sizeof(long))

struct cs1550_root_directory
{
	int nDirectories;	//How many subdirectories are in the root
						//Needs to be less than MAX_DIRS_IN_ROOT
	struct cs1550_directory
	{
		char dname[MAX_FILENAME + 1];	//directory name (plus space for nul)
		long nStartBlock;				//where the directory block is on disk
	} __attribute__((packed)) directories[MAX_DIRS_IN_ROOT];	//There is an array of these

	//This is some space to get this to be exactly the size of the disk block.
	//Don't use it for anything.
	char padding[BLOCK_SIZE - MAX_DIRS_IN_ROOT * sizeof(struct cs1550_directory) - sizeof(int)];
} ;

typedef struct cs1550_root_directory cs1550_root_directory;

struct cs1550_disk_block
{
	//The first 4 bytes will be the value 0xF113DA7A
	unsigned long magic_number;
	//And all the rest of the space in the block can be used for actual data
	//storage.
	char data[MAX_DATA_IN_BLOCK];
};

typedef struct cs1550_disk_block cs1550_disk_block;

//How many pointers in an inode?
#define NUM_POINTERS_IN_INODE (BLOCK_SIZE - sizeof(unsigned int) - sizeof(unsigned long))/sizeof(unsigned long)

struct cs1550_inode
{
	//The first 4 bytes will be the value 0xFFFFFFFF
	unsigned long magic_number;
	//The number of children this node has (either other inodes or data blocks)
	unsigned int children;
	//An array of disk pointers to child nodes (either other inodes or data)
	unsigned long pointers[NUM_POINTERS_IN_INODE];
};

typedef struct cs1550_inode cs1550_inode;
/*function prototypes*/
int file_in_dir(char* directory, char* filename, char* extension, cs1550_root_directory* root, FILE* file);
void get_root(cs1550_root_directory* root, FILE* file);
int dir_in_root(char* directory, cs1550_root_directory* root);
int find_free_in_bitmap(FILE* file);
int reset_bitmap(FILE* file, int location);

/*set the root node*/
void get_root(cs1550_root_directory* root, FILE* file){
	rewind(file);
	if(file == NULL)
		printf("file did not open!\n");
	fread(root, sizeof(cs1550_root_directory), 1, file);
}
/*Called to figure out if a directory is valid*/
int dir_in_root(char* directory, cs1550_root_directory* root) {
	int i;
	printf("Finding Directories\n");
	for (i=0; i <= root->nDirectories; i++) {
		printf("scanning %s for %s\n",root->directories[i].dname,directory);
		if (!strcmp(root->directories[i].dname, directory)){
			printf("Directory Name found!\n");
			return 1;
		}
	}
	return 0;
}

int file_in_dir(char* directory, char* filename, char* extension, cs1550_root_directory* root, FILE* file) {
	int i,j;
	cs1550_directory_entry dir;
	rewind(file);
	printf("file in dir\n");
	for (i=0; i <= root->nDirectories; i++) {
		/*find directory name*/
		if (!strcmp(root->directories[i].dname, directory)) {
			/*find the directory*/
			fseek(file,root->directories[i].nStartBlock,SEEK_SET);
			fread(&dir, sizeof(cs1550_root_directory), 1, file);
			for (j=0; j<= dir.nFiles; j++) {
				printf("filename exists? %d, extension? %d\n", !strcmp(dir.files[j].fname, filename),!strcmp(dir.files[j].fext, extension));
				if (!strcmp(dir.files[j].fname, filename) && !strcmp(dir.files[j].fext, extension)) {
					/*find the file*/
					printf("file size= %d\n",dir.files[j].fsize + 1);
					return (int) dir.files[j].fsize + 1;
				}
			}
		}
	}
	return 0;
}
/*
 * Called whenever the system wants to know the file attributes, including
 * simply whether the file exists or not.
 *
 * man -s 2 stat will show the fields of a stat structure
 */
static int cs1550_getattr(const char *path, struct stat *stbuf)
{
	int res = 0;
	int file_exists,dir_exists,filename_len;
	FILE * file;
	file_exists=0;
	/*Declare variables to find the path*/
	char* directory = calloc(1,MAX_FILENAME+1);
	char* filename = calloc(1,MAX_FILENAME+1);
	char* extension = calloc(1,MAX_EXTENSION+1);
	file = fopen(".disk","rb+");
	// memset(stbuf, 0, sizeof(struct stat));
	//is path the root dir?
	if (strcmp(path, "/") == 0) {
		stbuf->st_mode = S_IFDIR | 0755;
		stbuf->st_nlink = 2;
	}
	else {
		printf("Path is not root\n");
		struct cs1550_root_directory* root;
		root=calloc(1,sizeof(struct cs1550_root_directory)); /*create a pointer to the root node*/
		get_root(root,file); /*get the root node*/
		printf("root obtained\n");
		/*Figure out what the path is*/
		sscanf(path, "/%[^/]/%[^.].%s", directory, filename, extension);
		dir_exists = dir_in_root(directory,root);
		printf("dir_exists=%d\n",dir_exists);
		filename_len=strlen(filename); /*get the length of the filename.  If no filename is specified, this is 0*/
		printf("filename_len=%d\n",filename_len);
		if (filename_len) /*if there is a filename, check if it is valid*/
			file_exists = file_in_dir(directory,filename,extension,root,file); /*Check if the file is valid*/
		/* Check if name is subdirectory */
		if (dir_exists && filename_len==0) {
			printf("Is a directory\n");
			stbuf->st_mode = S_IFDIR | 0755;
			stbuf->st_nlink = 2;
			res = 0; //no error
		}
		else if(dir_exists && file_exists) {
			//Check if name is a regular file
			//regular file, probably want to be read and write
			printf("Is a file!\n");
			stbuf->st_mode = S_IFREG | 0666;
			stbuf->st_nlink = 1; //file links
			stbuf->st_size = file_exists - 1; //file size - make sure you replace with real size!
			res = 0; // no error
			//Else return that path doesn't exists
		}
		else {
			printf("DNE\n");
			res = -ENOENT;
		}
		free(root);
	}
	free(filename);
	free(directory);
	free(extension);
	fclose(file);
	return res;
}
/*
 * Called whenever the contents of a directory are desired. Could be from an 'ls'
 * or could even be when a user hits TAB to do autocompletion
 */
static int cs1550_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
			 off_t offset, struct fuse_file_info *fi)
{
	//Since we're building with -Wall (all warnings reported) we need
	//to "use" every parameter, so let's just cast them to void to
	//satisfy the compiler
	printf("readdir\n");
	(void) offset;
	(void) fi;
	int i,j;
	int dir_exists;
	/*Declare variables to find the path*/
	char* directory = calloc(1,MAX_FILENAME+1);
	char* filename = calloc(1,MAX_FILENAME+1);
	char* extension = calloc(1,MAX_EXTENSION+1);
	char full_filename[MAX_EXTENSION+MAX_FILENAME+1];
	cs1550_root_directory* root;
	FILE *file;
	file = fopen(".disk","rb+");
	cs1550_directory_entry dir;
	root=calloc(1,sizeof(struct cs1550_root_directory)); /*create a pointer to the root node*/
	get_root(root, file);
	printf("root obtained for readdir\n");
	//This line assumes we have no subdirectories, need to change
	if (strcmp(path, "/") == 0) {
		filler(buf, ".", NULL, 0);
		filler(buf,"..",NULL,0);
		for (i=1; i<=root->nDirectories;i++){
			printf("reading dir names for /: %s\n", root->directories[i].dname);
			/*if the directory name is not null, add it to filler*/
			if (root->directories[i].dname[0]) {
				filler(buf,root->directories[i].dname,NULL,0);
			}
		}
		return 0; /*success*/
	}
	sscanf(path, "/%[^/]/%[^.].%s", directory, filename, extension);
	dir_exists = dir_in_root(directory,root);
	printf("readdir dir_exists=%d\n",dir_exists);
	/*if the directory doesn't exist, or theres a filename, exit with error*/
	if (dir_exists==0 || strlen(filename)!=0)
		return -ENOENT;
	//the filler function allows us to add entries to the listing
	//read the fuse.h file for a description (in the ../include dir)
	filler(buf, ".", NULL, 0);
	filler(buf, "..", NULL, 0);
	/*find all the files in the specified directory*/
	//printf("root->nDirectories")
	for (i=1; i <= root->nDirectories; i++) {
		/*find directory name*/
		printf("Scanning for directories: %s\n",root->directories[i].dname );
		if (!strcmp(root->directories[i].dname, directory)) {
			/*find the directory*/
			printf("found dir!\n");
			rewind(file);
			fseek(file,root->directories[i].nStartBlock,SEEK_SET);
			fread(&dir, sizeof(cs1550_root_directory), 1, file);
			for (j=1; j<=dir.nFiles; j++) {
				/*find the file name and add it to the buffer*/
				strcpy(full_filename,dir.files[j].fname);
				/*if the file has an extension, add a . and the extension name */
				if(strlen(dir.files[j].fext)) {
					strcat(full_filename, ".");
					strcat(full_filename,dir.files[j].fext);
				}
				printf("full_filename = %s \n",full_filename);
				filler(buf,full_filename,NULL,0);
				memset(full_filename,0,MAX_FILENAME+MAX_EXTENSION+1);
			}
		}
	}
	fclose(file);
	free(filename);
	free(directory);
	free(extension);
	free(root);
	/*
	//add the user stuff (subdirs or files)
	//the +1 skips the leading '/' on the filenames
	filler(buf, newpath + 1, NULL, 0);
	*/
	return 0;
}

int find_free_in_bitmap(FILE* file) {
	char bitmap[BITMAP_SIZE];
	int i,j;
	/*get the number of files in each directory*/
	fseek(file,0,SEEK_SET);
	fseek(file, -1*BITMAP_SIZE, SEEK_END);
	/*get the bitmap*/
	fread(&bitmap, sizeof(cs1550_root_directory), 1, file);
	/*reset the filepointer*/
	fseek(file,0, SEEK_SET);
	fseek(file, -1*BITMAP_SIZE, SEEK_END);
	for (i=0; i<BITMAP_SIZE; i++) {
		for (j=0; j<8; j++) {
			/*if the bit is not set*/
			if(! ( (bitmap[i] & (1 << j) ) >> j ) ) {
				/*set the bit*/
				printf("setting bit\n");
				bitmap[i] |= 1 ;
				fwrite(bitmap,BITMAP_SIZE,1,file);
				/*return the location of the free block*/
				/*i is each char, so multiply it by 8 and add the offset j*/
				if (i*8+j > sizeof(cs1550_root_directory) )
					return i*8+j;
			}
		}
	}
	/*if theres no room, return -1*/
	return -1;
}

int reset_bitmap(FILE* file, int location) {
	char bitmap[BITMAP_SIZE];
	int i,j;
	/*get the number of files in each directory*/
	fseek(file,0,SEEK_SET);
	fseek(file, -1*BITMAP_SIZE, SEEK_END);
	/*get the bitmap*/
	fread(&bitmap, sizeof(cs1550_root_directory), 1, file);
	/*reset the filepointer*/
	fseek(file,0, SEEK_SET);
	fseek(file, -1*BITMAP_SIZE, SEEK_END);
	i = location/8;
	j = location % 8;
	/*reset the bit*/
	bitmap[i] &= ~(1 << j);
	fwrite(bitmap,BITMAP_SIZE,1,file);
	return 0;
}
/*
 * Creates a directory. We can ignore mode since we're not dealing with
 * permissions, as long as getattr returns appropriate ones for us.
 */
static int cs1550_mkdir(const char *path, mode_t mode)
{
	(void) path;
	(void) mode; /*ignore this*/
	char* directory = calloc(1,MAX_FILENAME+1);
	char* filename = calloc(1,MAX_FILENAME+1);
	char* extension = calloc(1,MAX_EXTENSION+1);
	int i;
	int location;
	cs1550_root_directory* root;
	cs1550_directory_entry dir;
	FILE* file;
	file = fopen(".disk","rb+");
	/*make sure we cannot mkdir the root*/
	if (strcmp(path, "/") == 0) {
		printf("This is the root\n");
		return -EEXIST;
	}
	root=calloc(1,sizeof(cs1550_root_directory)); /*create a pointer to the root node*/
	get_root(root,file); /*get the root node*/
	/*if the max number of directories is reached, dont let the user add more*/
	if (root->nDirectories==MAX_DIRS_IN_ROOT)
		return -EPERM;
	sscanf(path, "/%[^/]/%[^.].%s", directory, filename, extension);
	/*if this is in a subdirectory, this is not a valid operation*/
	printf("filename=%s\n",filename);
	if (strlen(filename)>0) {
		printf("You cannot make a directory inside another directory\n");
		return -EPERM;
	}
	if (strlen(directory) > MAX_FILENAME) {
		return -ENAMETOOLONG;
	}
	for (i=0; i<=root->nDirectories; i++) {
		printf("scanning mkdir%s\n",root->directories[i].dname);
		/*if the directory already exists return an error*/
		if(!strcmp(root->directories[i].dname,directory)) {
			printf("this directory already exists\n");
			return -EEXIST;
		}
	}
	location = find_free_in_bitmap(file);
	/*increment the number of directories in root*/
	root->nDirectories++;
	/*add the directory to dname and the start block*/
	strcpy(root->directories[root->nDirectories].dname,directory);
	root->directories[root->nDirectories].nStartBlock = location;
	dir.nFiles = 0;
	/*write out the directory struct and the root struct*/
	printf("writing directory %d of %lu\n",root->nDirectories,MAX_DIRS_IN_ROOT);
	fseek(file,location*BLOCK_SIZE, SEEK_SET);
	fwrite(&dir,sizeof(cs1550_directory_entry),1,file);
	fseek(file,0,SEEK_SET);
	fwrite(root,sizeof(cs1550_root_directory),1,file);
	fclose(file);
	free(filename);
	free(directory);
	free(extension);
	return 0;
}

/*
 * Removes a directory.
 */
static int cs1550_rmdir(const char *path)
{
	(void) path;
    return 0;
}

/*
 * Does the actual creation of a file. Mode and dev can be ignored.
 *
 */
static int cs1550_mknod(const char *path, mode_t mode, dev_t dev)
{
	(void) mode;
	(void) dev;
	(void) path;
	char* directory = calloc(1,MAX_FILENAME+1);
	char* filename = calloc(1,MAX_FILENAME+1);
	char* extension = calloc(1,MAX_EXTENSION+1);
	cs1550_root_directory* root;
	cs1550_directory_entry dir;
	int i,j,location,dir_location;
	cs1550_inode file_block;
	FILE * file;
	file = fopen(".disk","rb+");
	root=calloc(1,sizeof(cs1550_root_directory)); /*create a pointer to the root node*/
	/*if the path is root, return an error*/
	if (strcmp(path, "/") == 0) {
		printf("You can't make a file named /\n");
		return -EPERM;
	}
	sscanf(path, "/%[^/]/%[^.].%s", directory, filename, extension);
	/*if the length of the filename or the extension is too long return an error*/
	if (strlen(filename) > MAX_FILENAME || strlen(extension) > MAX_EXTENSION) {
		printf("Filename length is %d, max length is %d",strlen(filename),(int) MAX_FILENAME);
		printf("Extension length is %d, max length is %d",strlen(extension),(int) MAX_EXTENSION);
		return -ENAMETOOLONG;
	}
	/*if the filename is null, the user is trying to make a file in the root folder, which is not allowed*/
	if (!strlen(filename)) {
		printf("You cannot make a file in the root directory! \n");
		return -EPERM;
	}
	/*if the directory the file is to be created in does not exist, return an error*/
	get_root(root,file);
	if (!dir_in_root(directory,root)) {
		printf("You cannot make a file in a nonexistent directory! \n");
		return -EPERM;
	}
	/*find if the file already exists by traversing through the filesystem*/
	for (i=1; i<=root->nDirectories; i++) {
		/*if we find the directory, move into it*/
		printf("looking for directory %s, currently on %s\n",directory,root->directories[i].dname);
		if (!strcmp(directory,root->directories[i].dname)) {
			/*save the directory start block for later writing*/
			dir_location = root->directories[i].nStartBlock;
			/*get the directory struct from .disk*/
			rewind(file);
			fseek(file,root->directories[i].nStartBlock,SEEK_SET);
			fread(&dir, sizeof(cs1550_root_directory), 1, file);
			/*check if the directory has the maximum number of files already*/
			if(dir.nFiles == MAX_FILES_IN_DIR){
				printf("Max files in Directory reached!\n");
				return -EPERM;
			}
			/*loop through fname and fext to find out if the file already exists*/
			for (j=1; j<=dir.nFiles; j++) {
				if(!strcmp(dir.files[i].fname, filename) && !strcmp(dir.files[i].fext, extension))
				{
					printf("file already exists!\n");
					return -EEXIST;
				}
			}
		}
	}
	printf("getting location of free block\n");
	location = find_free_in_bitmap(file);
	printf("mknod location= %d\n",location);
	printf("Creating file #%d of %lu\n",dir.nFiles+1,MAX_FILES_IN_DIR);
	/*update the directory struct*/
	dir.nFiles++;
	strcpy(dir.files[dir.nFiles].fname,filename);
	strcpy(dir.files[dir.nFiles].fext,extension);
	dir.files[dir.nFiles].nStartBlock = location * BLOCK_SIZE;
	dir.files[dir.nFiles].nStartBlock = 0;
	/*Write the directory back to disk*/
	fseek(file,0, SEEK_SET);
	fseek(file,dir_location, SEEK_SET);
	fwrite(&dir,sizeof(cs1550_directory_entry),1,file);
	/*reset the file pointer, then seek to the free block to write the file*/
	fseek(file,0, SEEK_SET);
	fseek(file,location * BLOCK_SIZE, SEEK_SET);
	memset((void *)&file_block,0,sizeof(cs1550_inode)); /*fill the inode with 0's*/
	file_block.magic_number = 0xFFFFFFFF; /*set the magic number of a inode*/
	fwrite(&file_block,sizeof(cs1550_inode),1,file); /*write the inode*/
	fclose(file);
	free(filename);
	free(root);
	free(directory);
	free(extension);
	return 0;
}

/*
 * Deletes a file
 */
static int cs1550_unlink(const char *path)
{
    (void) path;
    return 0;
}

/*
 * Read size bytes from file into buf starting from offset
 *
 */
static int cs1550_read(const char *path, char *buf, size_t size, off_t offset,
			  struct fuse_file_info *fi)
{
	(void) buf;
	(void) offset;
	(void) fi;
	(void) path;
	char* directory = calloc(1,MAX_FILENAME+1);
	char* filename = calloc(1,MAX_FILENAME+1);
	char* extension = calloc(1,MAX_EXTENSION+1);
	cs1550_root_directory* root;
	cs1550_directory_entry dir;
	cs1550_disk_block block;
	int i,j,k,location,dir_location, file_location;
	cs1550_inode inode, new_node, new_node_check;
	FILE * file;
	file = fopen(".disk","rb+");
	root=calloc(1,sizeof(cs1550_root_directory)); /*create a pointer to the root node*/
	printf("///////////////////////////////////////read//////////////////////////////////\n");
	get_root(root, file);
	sscanf(path, "/%[^/]/%[^.].%s", directory, filename, extension);
	/*find if the file already exists by traversing through the filesystem*/
	for (i=0; i<=root->nDirectories; i++) {
		/*if we find the directory, move into it*/
		printf("read: looking for directory %s, currently on %s\n",directory,root->directories[i].dname);
		if (!strcmp(directory,root->directories[i].dname)) {
			/*save the directory start block for later writing*/
			dir_location = root->directories[i].nStartBlock;
			/*get the directory struct from .disk*/
			fseek(file,root->directories[i].nStartBlock,SEEK_SET);
			fread(&dir, sizeof(cs1550_root_directory), 1, file);
			/*loop through fname and fext to find out if the file already exists*/
			for (j=0; j<=dir.nFiles; j++) {
				if(!strcmp(dir.files[j].fname, filename) && !strcmp(dir.files[j].fext, extension))
				{
					/*set the size*/
					size = dir.files[j].fsize;
					file_location = dir.files[j].nStartBlock;
					printf("read: found file!\n");
					/*read in the inode*/
					fseek(file,0,SEEK_SET);
					fseek(file,dir.files[j].nStartBlock,SEEK_SET);
					fread(&inode, sizeof(cs1550_inode), 1, file);
				}
			}
		}
	}
		new_node=inode;
		/*loop through the children and start copying them to buff*/
		for (k=0; k<=new_node.children; k++) {
			printf("reading child %d of %d\n",k,new_node.children);
			/*if the last inode pointer is an inode, go to that inode */
			if (k+1 == NUM_POINTERS_IN_INODE) {
				/*reset the bitmap*/
				fseek(file,0,SEEK_SET);
				fseek(file,new_node.pointers[k],SEEK_SET);
				fread(&new_node_check, sizeof(cs1550_inode), 1, file);
				if (new_node.magic_number == 0xFFFFFFFF) {
					printf("read: found a new inode!\n");
					new_node = new_node_check;
					k = 0;
				}
			}
			else {
			/*copy the block to the buffer*/
			fseek(file,0,SEEK_SET);
			fseek(file,new_node.pointers[k],SEEK_SET);
			printf("pointer: %lu\n",new_node.pointers[k]);
			fread(&block, sizeof(cs1550_disk_block), 1, file);
			printf("found data: %s\n",block.data);
			strcat(buf,block.data);
			}
		}

	//check to make sure path exists
	//check that size is > 0
	//check that offset is <= to the file size
	//read in data
	//set size and return, or error

	return size;
}

/*
 * Write size bytes from buf into file starting from offset
 *
 */
static int cs1550_write(const char *path, const char *buf, size_t size,
			  off_t offset, struct fuse_file_info *fi)
{
	(void) buf;
	(void) offset;
	(void) fi;
	(void) path;
	char* directory = calloc(1,MAX_FILENAME+1);
	char* filename = calloc(1,MAX_FILENAME+1);
	char* extension = calloc(1,MAX_EXTENSION+1);
	cs1550_root_directory* root;
	cs1550_directory_entry dir;
	cs1550_disk_block block;
	int i,j,k,location,dir_location, file_location, curr_write_size, index;
	cs1550_inode inode, new_node, new_node_check;
	FILE * file;
	file_location=512;/*set this to after the root node, so bad stuff doesnt happen*/
	file = fopen(".disk","rb+");
	root=calloc(1,sizeof(cs1550_root_directory)); /*create a pointer to the root node*/
	printf("///////////////////////////////////////write//////////////////////////////////\n");
	get_root(root, file);
	sscanf(path, "/%[^/]/%[^.].%s", directory, filename, extension);
	/*find if the file already exists by traversing through the filesystem*/
	for (i=0; i<=root->nDirectories; i++) {
		/*if we find the directory, move into it*/
		printf("write: looking for directory %s, currently on %s\n",directory,root->directories[i].dname);
		if (!strcmp(directory,root->directories[i].dname)) {
			/*save the directory start block for later writing*/
			dir_location = root->directories[i].nStartBlock;
			/*get the directory struct from .disk*/
			fseek(file,root->directories[i].nStartBlock,SEEK_SET);
			fread(&dir, sizeof(cs1550_root_directory), 1, file);
			/*loop through fname and fext to find out if the file already exists*/
			for (j=0; j<=dir.nFiles; j++) {
				if(!strcmp(dir.files[j].fname, filename) && !strcmp(dir.files[j].fext, extension))
				{
					printf("write: found file!\n");
					/*read in the inode*/
					if (offset > dir.files[j].fsize)
						return -EFBIG;
					file_location = dir.files[j].nStartBlock;
					fseek(file,0,SEEK_SET);
					fseek(file,dir.files[j].nStartBlock,SEEK_SET);
					fread(&inode, sizeof(cs1550_inode), 1, file);
				}
			}
		}
	}
	new_node = inode;
	/*if the offset is set to 0, we will overwrite the current blocks*/
	if (!offset) {
		/*loop through the children and start resetting them*/
		for (k=0; k<new_node.children; k++) {
			printf("write resetting inode pointer %d of %d\n",k,new_node.children);
			/*if the last inode pointer is an inode, go to that inode */
			if (k+1 == NUM_POINTERS_IN_INODE) {
				/*reset the bitmap*/
				//reset_bitmap(file,new_node.pointers[k]);
				fseek(file,0,SEEK_SET);
				fseek(file,new_node.pointers[k],SEEK_SET);
				fread(&new_node_check, sizeof(cs1550_inode), 1, file);
				if (new_node.magic_number == 0xFFFFFFFF) {
					printf("write: found a new inode!\n",k,inode.children);
					new_node = new_node_check;
					k = 0;
				}
			}
			else
				reset_bitmap(file,inode.pointers[k]);
			new_node.pointers[k]=-1;
		}
		new_node.children = 0;
		/*print to the data blocks 508 bytes at a time*/
		curr_write_size = ((int) size)/MAX_DATA_IN_BLOCK + 1; //get the number of data blocks needed
		if (curr_write_size > NUM_POINTERS_IN_INODE)
			printf("write:adding inodes not implemented");
			/*start writing in diskblocks*/
		for (i=0; i < curr_write_size; i++) {
			new_node.children++;
			location = find_free_in_bitmap(file);
			printf("location: %d, location*512=%d\n", location,location * BLOCK_SIZE);
			memset((void *) &block,0,sizeof(cs1550_disk_block)); //reset the disk_block;
			block.magic_number =  0xF113DA7A; /*set the magic number of a inode*/
			index=i*MAX_DATA_IN_BLOCK;
			strncpy(block.data, buf+index, MAX_DATA_IN_BLOCK);
			fseek(file,0, SEEK_SET);
			fseek(file,location * BLOCK_SIZE, SEEK_SET);
			fwrite(&block,sizeof(cs1550_disk_block),1,file); /*write the disk_block*/
			new_node.pointers[k] = location * BLOCK_SIZE;
		}
		fseek(file,0, SEEK_SET);
		fseek(file,file_location, SEEK_SET);
		fwrite(&new_node,sizeof(cs1550_inode),1,file); /*write the disk_block*/

		}
		else
			printf("write: Append Not Implemented!\n");
	// check to make sure path exists
	// check that size is > 0
	// check that offset is <= to the file size
	// write data
	// set size (should be same as input) and return, or error
	fclose(file);
	free(filename);
	free(directory);
	free(extension);
	free(root);
	return size;
}

/******************************************************************************
 *
 *  DO NOT MODIFY ANYTHING BELOW THIS LINE
 *
 *****************************************************************************/

/*
 * truncate is called when a new file is created (with a 0 size) or when an
 * existing file is made shorter. We're not handling deleting files or
 * truncating existing ones, so all we need to do here is to initialize
 * the appropriate directory entry.
 *
 */
static int cs1550_truncate(const char *path, off_t size)
{
	(void) path;
	(void) size;

    return 0;
}


/*
 * Called when we open a file
 *
 */
static int cs1550_open(const char *path, struct fuse_file_info *fi)
{
	(void) path;
	(void) fi;
    /*
        //if we can't find the desired file, return an error
        return -ENOENT;
    */

    //It's not really necessary for this project to anything in open

    /* We're not going to worry about permissions for this project, but
	   if we were and we don't have them to the file we should return an error

        return -EACCES;
    */

    return 0; //success!
}

/*
 * Called when close is called on a file descriptor, but because it might
 * have been dup'ed, this isn't a guarantee we won't ever need the file
 * again. For us, return success simply to avoid the unimplemented error
 * in the debug log.
 */
static int cs1550_flush (const char *path , struct fuse_file_info *fi)
{
	(void) path;
	(void) fi;
	return 0; //success!
}


//register our new functions as the implementations of the syscalls
static struct fuse_operations hello_oper = {
    .getattr	= cs1550_getattr,
    .readdir	= cs1550_readdir,
    .mkdir	= cs1550_mkdir,
	.rmdir = cs1550_rmdir,
    .read	= cs1550_read,
    .write	= cs1550_write,
	.mknod	= cs1550_mknod,
	.unlink = cs1550_unlink,
	.truncate = cs1550_truncate,
	.flush = cs1550_flush,
	.open	= cs1550_open,
};

//Don't change this.
int main(int argc, char *argv[])
{
	return fuse_main(argc, argv, &hello_oper, NULL);
}
